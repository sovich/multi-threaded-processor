<?php
declare(strict_types=1);

namespace MultiThreadedProcess;

use Closure;
use Generator;
use SplObjectStorage;
use Symfony\Component\Process\Process;

final class MultiThreadedProcess
{
    /** @var SplObjectStorage<Process> */
    private SplObjectStorage $storage;

    /** @var SplObjectStorage<Process> */
    private SplObjectStorage $processing;

    /** @var SplObjectStorage<Process> */
    private SplObjectStorage $finished;

    private int $threaded;

    private Closure $started;

    private Closure $failed;

    private Closure $success;

    /**
     * @param Process[]    $processing
     * @param Closure(Process $proces): void|null $started
     * @param Closure(Process $proces): void|null $failed
     * @param Closure(Process $proces): void|null $success
     */
    public function __construct(int $threaded, array $processing = [], Closure $started = null, Closure $success = null, Closure $failed = null)
    {
        $this->storage    = new SplObjectStorage();
        $this->processing = new SplObjectStorage();
        $this->finished   = new SplObjectStorage();

        foreach ($processing as $process) {
            $this->add($process);
        }

        $this->threaded = $threaded;
        $this->started  = $started ?: static fn (Process $process) => null;
        $this->failed   = $failed ?: static fn (Process $process) => null;
        $this->success  = $success ?: static fn (Process $process) => null;
    }

    public function add(Process $process): self
    {
        $this->storage->attach($process);

        return $this;
    }

    public function runBatch(): bool
    {
        $this->check();
        $this->run();

        return $this->processing->count() > 0;
    }

    public function getProcessingCount(): int
    {
        return $this->processing->count();
    }

    public function getWaitingRunCount(): int
    {
        return $this->storage->count();
    }

    public function getFinishedCount(): int
    {
        return $this->finished->count();
    }

    /**
     * @return Generator<int, Process>
     */
    public function iterate(int $sleep = 1000): Generator
    {
        $this->run();
        while ($this->getProcessingCount() > 0) {
            usleep($sleep);
            foreach ($this->processing as $process) {
                if (!$process->isRunning()) {
                    $this->moveToFinished($process);
                    $this->run();
                    yield $process;
                }
            }
        }
    }

    private function check(): void
    {
        foreach ($this->processing as $item) {
            if (!$item->isRunning()) {
                $this->moveToFinished($item);
            }
        }
    }

    private function run(): void
    {
        while ($this->getProcessingCount() < $this->threaded && $this->getWaitingRunCount() > 0) {
            $process = $this->storage->current();
            $this->storage->detach($process);
            $this->processing->attach($process);
            $process->start();
            ($this->started)($process);
        }
    }

    private function moveToFinished(Process $process): void
    {
        $this->processing->detach($process);

        $process->isSuccessful() ? ($this->success)($process) : ($this->failed)($process);

        $this->finished->attach($process);
    }
}
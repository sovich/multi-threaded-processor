<?php
declare(strict_types=1);

namespace MultiThreadedProcess\Tests;

use MultiThreadedProcess\MultiThreadedProcess;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\Process;

final class MultiThreadedProcessTest extends TestCase
{
    public function testRunBatchProcess(): void
    {
        $threadsProcess = new MultiThreadedProcess(2);

        $process1 = Process::fromShellCommandline('echo test');
        $process2 = Process::fromShellCommandline('echo test');
        $process3 = Process::fromShellCommandline('echo test');

        $threadsProcess->add($process1);
        $threadsProcess->add($process2);
        $threadsProcess->add($process3);

        while ($threadsProcess->runBatch()) {
            usleep(1000);
        }

        self::assertEquals(0, $threadsProcess->getWaitingRunCount());
        self::assertEquals(0, $threadsProcess->getProcessingCount());
        self::assertEquals(3, $threadsProcess->getFinishedCount());

        self::assertEquals(false, $process1->isRunning());
        self::assertEquals(false, $process2->isRunning());
        self::assertEquals(false, $process3->isRunning());

        self::assertEquals(true, $process1->isSuccessful());
        self::assertEquals(true, $process2->isSuccessful());
        self::assertEquals(true, $process3->isSuccessful());
    }

    public function testIterableProcess(): void
    {
        $process1 = Process::fromShellCommandline('echo test');
        $process2 = Process::fromShellCommandline('echo test');
        $process3 = Process::fromShellCommandline('echo test');
        $process4 = Process::fromShellCommandline('echo test');

        $countProcess  = 4;
        $finishedCount = 0;
        $threadCount   = 2;

        $threadsProcess = new MultiThreadedProcess($threadCount, [$process1, $process2, $process3]);
        $threadsProcess->add($process4);

        self::assertEquals($countProcess, $threadsProcess->getWaitingRunCount());
        self::assertEquals($countProcess, $threadsProcess->getWaitingRunCount());
        self::assertEquals($finishedCount, $threadsProcess->getFinishedCount());

        foreach ($threadsProcess->iterate() as $process) {
            $finishedCount++;

            self::assertInstanceOf(Process::class, $process);
            self::assertEquals(false, $process->isRunning());
            self::assertEquals(true, $process->isSuccessful());

            $expectedRunnable = min(($countProcess - $finishedCount), $threadCount);
            self::assertEquals($expectedRunnable, $threadsProcess->getProcessingCount());
            self::assertEquals($countProcess - $finishedCount - $expectedRunnable, $threadsProcess->getWaitingRunCount());
            self::assertEquals($finishedCount, $threadsProcess->getFinishedCount());
        }

        self::assertEquals(0, $threadsProcess->getWaitingRunCount());
        self::assertEquals(0, $threadsProcess->getProcessingCount());
        self::assertEquals(4, $threadsProcess->getFinishedCount());

        self::assertEquals(false, $process1->isRunning());
        self::assertEquals(false, $process2->isRunning());
        self::assertEquals(false, $process3->isRunning());
        self::assertEquals(false, $process4->isRunning());

        self::assertEquals(true, $process1->isSuccessful());
        self::assertEquals(true, $process2->isSuccessful());
        self::assertEquals(true, $process3->isSuccessful());
        self::assertEquals(true, $process4->isSuccessful());
    }

    public function testCallback(): void
    {
        $process1 = Process::fromShellCommandline('echo test');
        $process2 = Process::fromShellCommandline('echo test | false');
        $process3 = Process::fromShellCommandline('echo test | false');
        $process4 = Process::fromShellCommandline('echo test | false');

        $failedCount  = 0;
        $successCount = 0;
        $startedCount = 0;

        $started = function (Process $process) use (&$startedCount) {
            self::assertTrue($process->isRunning());
            $startedCount++;
        };

        $success = function (Process $process) use (&$successCount) {
            self::assertTrue(!$process->isRunning() && $process->isSuccessful());
            $successCount++;
        };

        $failed  = function (Process $process) use (&$failedCount) {
            self::assertTrue(!$process->isRunning() && !$process->isSuccessful());
            $failedCount++;
        };

        $threadsProcess = new MultiThreadedProcess(2, [], $started, $success, $failed);
        array_map([$threadsProcess, 'add'], [$process1, $process2, $process3, $process4]);

        self::assertEquals(0, $successCount);
        self::assertEquals(0, $failedCount);
        self::assertEquals(0, $startedCount);

        iterator_to_array($threadsProcess->iterate());

        self::assertEquals(1, $successCount);
        self::assertEquals(3, $failedCount);
        self::assertEquals(4, $startedCount);
    }
}